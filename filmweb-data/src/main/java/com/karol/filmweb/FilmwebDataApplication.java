package com.karol.filmweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilmwebDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilmwebDataApplication.class, args);
	}
}
