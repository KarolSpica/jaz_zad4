package com.karol.filmweb.controllers;

import com.karol.filmweb.domain.Comment;
import com.karol.filmweb.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CommentController {

    @Autowired
    private CommentService commentService;


    @RequestMapping("/movies/{id}/comments")
    public List<Comment> getAllComments(@PathVariable String id) {
        return commentService.getAllComments(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/movies/{id}")
    public void addComment(@RequestBody Comment comment, @PathVariable String id) {
        commentService.addComment(comment, id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/movies/{movieId}/{id}")
    public void deleteComment(@PathVariable long id) {
        commentService.deleteComment(id);
    }

}
