package com.karol.filmweb.controllers;

import com.karol.filmweb.services.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RatingController {

    @Autowired
    private RatingService ratingService;

    @RequestMapping(method = RequestMethod.POST, value = "/movies/{movieId}/{rating}")
    public void addRating(@PathVariable String movieId, @PathVariable int rating) {
        ratingService.addRating(movieId, rating);
    }

    @RequestMapping("/movies/{movieId}/rating")
    public String getAverageRatting(@PathVariable String movieId){
        return ratingService.getAverageRating(movieId);
    }
}
