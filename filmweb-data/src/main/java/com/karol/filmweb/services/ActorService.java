package com.karol.filmweb.services;

import com.karol.filmweb.data.ActorRepository;
import com.karol.filmweb.domain.Actor;
import com.karol.filmweb.domain.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ActorService {

    @Autowired
    private ActorRepository actorRepository;

    public void addActor(Actor actor) {
        actorRepository.save(actor);
    }

    public List<Actor> getAllActors() {
        List<Actor> actors = new ArrayList<>();
        actorRepository.findAll()
                .forEach(actors::add);
        return  actors;
    }

    public Actor getActorById(long id) {
       return actorRepository.findById(id).orElse(null);
    }


    public List<Movie> getAllActorMovies(long actorId) {
      Actor actor = actorRepository.findById(actorId).orElse(null);
      return actor.getMovies();
    }
    public void addActorMovie(long id, Movie movie) {
        Actor actor = actorRepository.findById(id).orElse(null);
        actor.getMovies().add(movie);
        actorRepository.save(actor);
    }
}
