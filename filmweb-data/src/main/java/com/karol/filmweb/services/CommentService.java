package com.karol.filmweb.services;

import com.karol.filmweb.data.CommentRepository;
import com.karol.filmweb.data.MovieRepository;
import com.karol.filmweb.domain.Comment;
import com.karol.filmweb.domain.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private MovieRepository movieRepository;

    public void deleteComment(Long id) {
        commentRepository.deleteById(id);
    }

    public void addComment(Comment comment, String movieId) {
        Movie movie = getMovieFromDatabase(movieId);
        movie.getComments().add(comment);
        movieRepository.save(movie);
    }

    public List<Comment> getAllComments(String movieId) {
        return getMovieFromDatabase(movieId).getComments();
    }

    public Movie getMovieFromDatabase(String movieId) {
        return movieRepository.findById(movieId).orElse(null);
    }
}
