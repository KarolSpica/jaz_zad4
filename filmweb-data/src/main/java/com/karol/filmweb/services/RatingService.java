package com.karol.filmweb.services;

import com.karol.filmweb.data.MovieRepository;
import com.karol.filmweb.domain.Movie;
import com.karol.filmweb.domain.Rating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.List;

@Service
public class RatingService {

    @Autowired
    private MovieRepository movieRepository;

    public void addRating(String movieId,int rating) {
        Movie movie = getMovieFromDatabase(movieId);
        movie.getRatings().add(new Rating(rating));
        movieRepository.save(movie);
    }

    public String getAverageRating(String movieId) {
        return countAverageRating(getMovieFromDatabase(movieId).getRatings());
    }

    private String countAverageRating(List<Rating> ratings){
        long sumOfRatings =0;
        long numberOfRatings = 0;
        for(Rating rating : ratings) {
            sumOfRatings += rating.getRating();
            numberOfRatings++;
        }
        return new DecimalFormat("#.##").format((double)sumOfRatings/numberOfRatings);
    }

    public Movie getMovieFromDatabase(String movieId) {
        return movieRepository.findById(movieId).orElse(null);
    }



}
